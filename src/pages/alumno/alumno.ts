import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LoginProvider } from '../../providers/login/login';
import { AlumnoProvider } from '../../providers/alumno/alumno';
import { Chart } from 'chart.js';

/*
 * Generated class for the AlumnoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-alumno',
  templateUrl: 'alumno.html',
})
export class AlumnoPage {
  chart: any = null;
    promedioRamos: number;
    ramosTomados: number;
    nombre: string;
    DSRamos : number;
    Ranking : number;
    promedio2 : number;
    ds2 : number;
  

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public httpClient: HttpClient,
    public loginProvider: LoginProvider,
    public alumnoProvider: AlumnoProvider) {

    console.log(loginProvider);
    
  }

  /*ngOnInit() {
    this.alumnoProvider
        .loggin(this.loginProvider.rut, this.loginProvider.apiKey).subscribe((data:any)=>{
        console.log(data);
        this.firstName = data.firstName;
        })
  }*/

  ngOnInit() {
        /*console.log();
        let rut = this.loginProvider.rut;
        
        let url = `https://api.sebastian.cl/academia/api/v1/courses/students/${rut}/`;
        const httpOptions = {
            headers: new HttpHeaders({ 'X-API-KEY': this.loginProvider.apiKey})
        };
        return this.httpClient.get(url,httpOptions).subscribe((data:any)=>{
          this.ramosTomados = data.length;
          this.promedioRamos= data.gender;

        }
        );*/

        let rut = this.loginProvider.rut;
        let url = `https://api.sebastian.cl/academia/api/v1/courses/students/${rut}/`;
        let url2 = `https://api.sebastian.cl/academia/api/v1/rankings/${rut}/`;

        const httpOptions = {
            headers: new HttpHeaders({ 'X-API-KEY': this.loginProvider.apiKey})
        };

        this.httpClient
            .get(url, httpOptions)
            .subscribe(
                (result: [any]) => {
                    this.nombre = result[0].student.firstName + ' ' + result[0].student.lastName;
                    this.ramosTomados = result.length;
                    this.promedioRamos = this.calculateAvg(result);
                    this.DSRamos = this.calculateSD(result,this.promedioRamos);

                    let charData = this.calculateGraphData(result);

                    let canvas = document.getElementById("canvas");
                    this.chart = new Chart(canvas, {
                        type: 'pie',
                        data: {
                            datasets: [{
                                data: charData,
                                backgroundColor: [
                                    'rgb(75, 192, 192)', // Verde
                                    'rgb(255, 99, 132)' // Rojo
                                ]
                            }],
                            labels: [ 'Aprobados', 'Reprobados']
                        }
                    });
                });


        this.httpClient
            .get(url2, httpOptions)
            .subscribe(
                (result: [any]) => {
                    this.Ranking = result.position;
                    /*this.promedio2 = result.average;
                    this.ds2 = result.stddev;*/
                });
            
    }
    calculateAvg(data : [any]) {
      let avg : number = 0;

      data.forEach(data => {
          avg = avg + data.grade;
      });
      avg = avg / data.length;


      return avg;
  }
  calculateSD(data : [any], number) {
    let DSN : number = 0;
    let DSD : number = 0;
    let V : number = 0;
    let DS : number = 0;
    data.forEach(data => {
        DSN = DSN + ((data.grade-number)*(data.grade-number));
    });
    
    DSD = data.length - 1;
    V = DSN/DSD;

    DS = Math.sqrt(V);

    return DS;

}

calculateGraphData(data : [any]) {
    let aprobado = 0;
    let reprobado = 0;

    data.forEach(data => {
        if (data.status == "APROBADO") {
            aprobado++;
        }
        else{
            reprobado++;
        }
    });

    return [aprobado, reprobado];
}



}


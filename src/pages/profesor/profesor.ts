import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoginProvider } from '../../providers/login/login';
import { HttpClient, HttpHeaders } from '@angular/common/http';

/**
 * Generated class for the ProfesorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profesor',
  templateUrl: 'profesor.html',
})
export class ProfesorPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public loginProvider: LoginProvider,
    public httpClient: HttpClient) {
  }
  ngOnInit() {
    /*console.log();
    let rut = this.loginProvider.rut;
    
    let url = `https://api.sebastian.cl/academia/api/v1/courses/students/${rut}/`;
    const httpOptions = {
        headers: new HttpHeaders({ 'X-API-KEY': this.loginProvider.apiKey})
    };
    return this.httpClient.get(url,httpOptions).subscribe((data:any)=>{
      this.ramosTomados = data.length;
      this.promedioRamos= data.gender;

    }
    );*/

    let rut = this.loginProvider.rut;
    let url = `https://api.sebastian.cl/academia/api/v1/courses/teachers/${rut}/stats`;

    const httpOptions = {
        headers: new HttpHeaders({ 'X-API-KEY': this.loginProvider.apiKey})
    };

    this.httpClient
        .get(url, httpOptions)
        .subscribe(
            (result: [any]) => {
                this.nombre = result[0].teacher.firstName + ' ' + result[0].teacher.lastName;
                this.ramosImpartidos = result.length;
                this.promedioRamos = this.calculateAvg(result);
                this.DSRamos = this.calculateSD(result,this.promedioRamos);

              });

        
}
calculateAvg(data : [any]) {
  let avg : number = 0;

  data.forEach(data => {
      avg = avg + data.average;
  });
  avg = avg / data.length;


  return avg;
}
calculateSD(data : [any], number) {
let DSN : number = 0;
let DSD : number = 0;
let V : number = 0;
let DS : number = 0;
data.forEach(data => {
    DSN = DSN + ((data.average-number)*(data.average-number));
});

DSD = data.length - 1;
V = DSN/DSD;

DS = Math.sqrt(V);

return DS;

}
  // pie

  num1 = 0.6;
  public pieChartLabels:string[] = ['Aprobados', 'Reprobados'];
  public pieChartData:Number[] = [80,20];
  public pieChartType:string = 'pie';

  // events
  public chartClicked(e:any):void {
    console.log(e);
  }
  
  public chartHovered(e:any):void {
    console.log(e);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfesorPage');
  }

}

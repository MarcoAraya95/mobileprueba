import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GrafalumnoPage } from './grafalumno';

@NgModule({
  declarations: [
    GrafalumnoPage,
  ],
  imports: [
    IonicPageModule.forChild(GrafalumnoPage),
  ],
})
export class GrafalumnoPageModule {}

import { Component } from '@angular/core';
import { NavController, Alert } from 'ionic-angular';
import { AlumnoPage } from '../alumno/alumno';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { AlertController } from 'ionic-angular'

import { LoginProvider } from '../../providers/login/login';
import { ListalumnoPage } from '../listalumno/listalumno';
import { Storage } from '@ionic/storage'
import { LOCALE_DATA } from '@angular/common/src/i18n/locale_data';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  myForm: FormGroup;


  constructor(public navCtrl: NavController,
              public loginprovider: LoginProvider,
              public formBuilder: FormBuilder,
              public alertcontrol: AlertController) {

                this.myForm = this.formBuilder.group({
                  rut: ['', Validators.compose([Validators.pattern('^([0-9]{1}|[0-9]{2})(\.[0-9]{3}\.[0-9]{3}-)([a-zA-Z]{1}$|[0-9]{1}$)'),
                  Validators.required])],
                  password: ['', Validators.required]
                });
  }


  login() {
    this.loginprovider
        .loggin(this.myForm.value.rut,this.myForm.value.password).subscribe((data:any)=>{
        console.log(data);
        localStorage.setItem("apiKey", data.apikey);
        this.loginprovider.role = data.role;
        this.loginprovider.email = data.email;
        this.navCtrl.push(ListalumnoPage, localStorage.getItem("apiKey"));
        }, this.failogin)
  }



  failogin(error) {
    console.log(error);
    this.alertcontrol.create({
      title: 'holi',
      message: 'ionda'
    })
    
  }

}

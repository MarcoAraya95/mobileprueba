import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListalumnoPage } from './listalumno';

@NgModule({
  declarations: [
    ListalumnoPage,
  ],
  imports: [
    IonicPageModule.forChild(ListalumnoPage),
  ],
})
export class ListalumnoPageModule {}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AvisoprofePage } from './avisoprofe';

@NgModule({
  declarations: [
    AvisoprofePage,
  ],
  imports: [
    IonicPageModule.forChild(AvisoprofePage),
  ],
})
export class AvisoprofePageModule {}

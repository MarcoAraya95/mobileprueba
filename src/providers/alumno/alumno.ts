import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the AlumnoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

@Injectable()
export class AlumnoProvider {
    rut : null;
    role : null;
    email : null; 
    apikey : null;

  constructor(public httpclient: HttpClient) {
    console.log('Hello AlumnoProvider Provider');
  }

  loggin(rut: string, apikey: string){
    let url = 'https://api.sebastian.cl/academia/api/v1/students/'+rut;
    let info = {rut: rut, apiKey: apikey};
    let httpOptions = {
      headers: new HttpHeaders({ 'X-API-KEY': apikey})
  };
    return this.httpclient.get(url,httpOptions)
  }

}

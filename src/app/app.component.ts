import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { AlumnoPage } from '../pages/alumno/alumno';
import { ProfesorPage } from '../pages/profesor/profesor';
import { AvisoprofePage } from '../pages/avisoprofe/avisoprofe';
import { ForgotPage } from '../pages/forgot/forgot';
import { GrafalumnoPage } from '../pages/grafalumno/grafalumno';
import { ListalumnoPage } from '../pages/listalumno/listalumno';
import { ListprofesorPage } from '../pages/listprofesor/listprofesor';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage},
      { title: 'List', component: ListPage },
      { title: 'Alumno', component: AlumnoPage },
      { title: 'Profesor', component: ProfesorPage },
      { title: 'Aviso Profe', component: AvisoprofePage },
      { title: 'Olvide Contraseña', component: ForgotPage },
      { title: 'Grafico Alumno', component: GrafalumnoPage },
      { title: 'Listado Alumno', component: ListalumnoPage },
      { title: 'Listado Profesor', component: ListprofesorPage },



    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}

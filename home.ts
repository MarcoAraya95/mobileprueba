import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AlumnoPage } from '../alumno/alumno';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController } from 'ionic-angular'

import { LoginProvider } from '../../providers/login/login';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  myForm: FormGroup;

  constructor(public navCtrl: NavController,
              public loginprovider: LoginProvider,
              public formBuilder: FormBuilder,
              public alertcontrol: AlertController) {

                this.myForm = this.formBuilder.group({
                  rut: [''],
                  password: ['']
                });
  }

  login() {
    this.loginprovider
        .loggin(this.myForm.value.rut,this.myForm.value.password).subscribe((data:any)=>{
        console.log(data);
        this.loginprovider.apikey = data.apikey;
        this.loginprovider.rut = data.rut;
        this.loginprovider.role = data.role;
        this.loginprovider.email = data.email;
        this.navCtrl.push(AlumnoPage);
        }, this.failogin)
  }

  failogin(error) {
    console.log(error);
  }

}
